<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;

use App\Models\Product;
use Illuminate\Http\Request;

class FrontendController extends Controller
{ 
    public function index(){
        $products = Product::latest()->paginate(6);
        
        return view('welcome',compact('products'));
    }
    public function view($id) {
        $product = Product::findOrFail($id);
        return view('view',compact('product'));        
    }
}
