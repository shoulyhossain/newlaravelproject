<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    // public function authorize()
    // {
    //     return false;
    // }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {

        $imageValidateRules = 'mimes:jpg,png|min:5|max:2048';
        $titleValidate ='max:30';
        if($this->isMethod('post')){
            $imageValidateRules = 'required|mimes:jpg,png|min:5|max:2048';
            $titleValidate ='required|max:30|unique:products,title';
        }

        return [
        
                'title' => $titleValidate,
                'description' => 'required|min:20|max:200',
                'price' => 'required|numeric',
                'image' => $imageValidateRules,

           
        ];
    }
}
