<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductRequest;
use App\Models\Product;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Image;

class ProductsController extends Controller
{
    public function index()
    {
        $products =  Product::orderBy('id', 'desc')->get();
        return view('backend/products/index', compact('products'));
    }
    public function create()
    {

        return view('backend/products/create');
    }
    public function store(ProductRequest $request)
    {

        $requestData = $request->all();
        try {


            if ($request->hasFile('image')) {
                $file = $request->file('image');
                $fileName = time().'.'.$file->getClientOriginalExtension();

                Image::make($request->file('image'))
                    ->resize(300, 200)
                    ->save(storage_path().'/app/public/products/'.$fileName);
                $requestData['image'] = $fileName;
            }

            Product::create($requestData);
            return redirect()->route('dashboard.products')->withMessage('Successfully Saved !');
        } catch (QueryException $e) {
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }
    public function show($id)
    {
        $product = Product::findOrFail($id);
        // dd($product);
        return view('backend/products/view', compact('product'));
    }
    public function edit($id)
    {
        $product = Product::findOrFail($id);
        // dd($product);
        return view('backend/products/edit', compact('product'));
    }
    public function update(ProductRequest $request, $id)
    {
        $requestData = $request->all();
        try {
            $product = Product::findOrFail($id);

            if ($request->hasFile('image')) {
                $file = $request->file('image');
                $fileName = time().'.'.$file->getClientOriginalExtension();
                Image::make($request->file('image'))
                    ->resize(300, 200)
                    ->save(storage_path().'/app/public/products/'.$fileName);
                $requestData['image'] = $fileName;
            } else {
                $requestData['image'] = $product->image;
            }

            // $requestData['updated_by'] = auth()->id();
            $product->update($requestData);

            return redirect()->route('dashboard.products')->withMessage('Successfully Updated !');
        } catch (QueryException $q) {
            return redirect()->back()->withInput()->withErrors($q->getMessage());
        }
    }
    public function destroy($id)
    {
        Product::findOrFail($id)->delete();
        // dd($product);
        return redirect()->route('dashboard.products')->with('message', 'Product deleted!');
    }
}
