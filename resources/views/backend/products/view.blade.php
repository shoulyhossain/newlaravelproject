<x-backend.layouts.master>
<h3>Product details</h3>
    <p>name: {{$product->title}}</p>
    <p>price: {{$product->price}}</p>

    <p>description: {{$product->description}}</p>

    <p>Created at: {{$product->created_at}}</p>
    <br>

   <img src="{{asset('storage/products/'.$product->image)}}" alt="No image in database">

    <br>

   <a class="btn btn-success" href="{{route('dashboard.products')}}"> Go back to product list</a>
   <a href="{{route('dashboard.products.edit', ['id'=>$product->id])}}" class="btn btn-primary">Edit</a>
</x-backend.layouts.master>