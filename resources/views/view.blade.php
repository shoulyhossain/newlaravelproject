<x-frontend.layouts.master>
    <div class="height d-flex justify-content-center align-items-center">

        <div class="card p-3 mt-3 mb-4">
            <div class="image-fluid text-center">
                <img src="{{asset('storage/products/'.$product->image)}}" width="200">
            </div>
            <div class="d-flex justify-content-between align-items-center ">
                <div class="mt-2">
                    <h4 class="text-uppercase"></h4>
                    <div class="mt-5">
                        <h1 class="main-heading mt-0">{{$product->title}}</h1>
                        <div class="d-flex flex-row user-ratings">
                            <div class="ratings">
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                            </div>
                       
                        </div>
                    </div>
                </div>
              
            </div>

            <div class="d-flex justify-content-between align-items-center mt-2 mb-2">
                <span>{{$product->price}}</span>
                <div class="colors">
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                </div>

            </div>


            <p>{{$product->description}} </p>

            <button class="btn btn-danger">Add to cart</button>
        </div>

    </div>


</x-frontend.layouts.master>
