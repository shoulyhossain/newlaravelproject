<?php

namespace App\Http\Controllers;

use App\Http\Requests\ColorRequest;
use App\Models\Color;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

class ColorsController extends Controller
{
    public function index()
    {
        $colors =  Color::orderBy('id','desc')->get();
        return view('backend/colors/index', compact('colors'));
    }
    public function create()
    {
        return view('backend/colors/create');
    }
    public function store(Request $request)
    {
        try {
            $request->validate(
                [
                    'title' => 'required|max:30|unique:colors,title',
                    'description' => 'required|min:20|max:200'   
                ]
            );
            Color::create($request->all());
            return redirect()->route('dashboard.colors');
        } catch (QueryException $q) {
            return redirect()->back()->withInput()->withErrors($q->getMessage());
        }
    }
    public function show($id)
    {
        $color = Color::findOrFail($id);
        // dd($color);
        return view('backend/colors/view',compact('color'));
    }
    public function edit($id)
    {
        $color = Color::findOrFail($id);
        // dd($color);
        return view('backend/colors/edit',compact('color'));
    }
    public function update(ColorRequest $request ,$id)
    {
        try {
            
            Color::findOrFail($id)->update($request->all());
            return redirect()->route('dashboard.colors')->with('message','Color details updated!');
        } catch (QueryException $q) {
            return redirect()->back()->withInput()->withErrors($q->getMessage());
        }
    }
    public function destroy($id)
    {
        Color::findOrFail($id)->delete();
        // dd($color);
        return redirect()->route('dashboard.colors')->with('message','Color deleted!');
    }
}
